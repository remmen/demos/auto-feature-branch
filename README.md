# auto-feature-branch

This is a proof of concept demo application which will create a new CD twin repo for each branch named feature/*<branch name>*
The twin CD branch based on "auto-feature-branch-deploy" will then roll out an argocd application.

The CD repository can be found here: https://gitlab.com/remmen/demos/auto-feature-branch-deploy

This poc code is based on 
- ArgoCD (https://argoproj.github.io)
- Harbor docker registry (https://goharbor.io/)
- Bitnami selaed-secrets (https://github.com/bitnami-labs/sealed-secrets)
- Kubernetes cluster with rancher (https://www.rancher.com/) 

```Please note, that this is a simplified proof of concept code!```

## How it works

![alt text](feature-branch-demo.gif)

## Pipeline process

![alt text](pipeline_process.png){width=60%}

1. The developer creates a new feature branch named "feature/my-dev-task"
2. Once pushed to the git branch, a twin branch with the same name is created within the CD repository
3. The CI pipeline builds the artifact and loads this into the docker registry
4. After the artifact is successfully built and uploaded, the corresponding image tag is updated in the CD branch.
5. ArgoCD registers the change in the GIT branch based on the hashtag
6. ArgoCD applies the changes to the kubernetes cluster

The feature app will be available at https://my-dev-task.feature.yourdomain.ch

## Branch name restrictions

As the branch name is used as the ingress prefix (without the feature/) non valid DNS characters are not allowed. There is no check, but upper case letters will be transformed to lower case.

## Prerequisites and variables

In order to work, you'll need a wildcard DNS entry pointing to you k8s-cluster. In this poc example code, you'll need a  *.demo.<your.domain.ch> entry.

Communication between the two repositories is based on tokens. Once created, set them among the other variables under *settings/ci_cd* within your GitLab environment.

| variable  | description  |
|---|---|
| **GIT_URL** | git server url |
| **GIT_DEPLOY_REPO** | name of the CD repo |
| **GIT_USER** | something (it doensn't matter), the token matters |
| **GIT_TOKEN** | git token |
| **GIT_PROJECT_URL** | CD repository url |
| **REGISTRY_URL** | docker registry url |
| **REGISTRY_TOKEN** | registry token |
| **REGISTRY_USER** | registry user |
| **REGISTRY_PROJECT** | harbor registry project |

## Gitlab environments

The state of each branch is tracked through a gitlab environment. Once the state switches from available to stopped (either a merge or the branch is deleted) the *on_stop* action will clean up the twin branch. For more information see https://docs.gitlab.com/ee/ci/environments/#run-a-pipeline-job-when-environment-is-stopped

## Application

To demonstrate this poc I used a simple nuxt 3 application. The static output (html) of the application is then packed into a simple generic nginx container.

## License

This code is licensed under the CC0 1.0 Universal licenses. Read license.txt for more information.